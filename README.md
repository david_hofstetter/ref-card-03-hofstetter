# Einleitung 
Uns wurde die Aufgabe gestellt eine CI/CD Pipelines zu verstehen mit dieser sollte ich dann den Container-Build automatisieren und die Applikation in meiner AWS Umgebung laufen lassen. Gestellt wurde mir die Aufgabe von Herr Oberle im Modul 347 an der BBW. Das Abgabedatum ist der 06.06.2023.

## Pipeline
Eine CI/CD-Pipeline ist ein automatisierter Prozess in der Softwareentwicklung, der Continuous Integration und Continuous Deployment kombiniert. Dabei wird der Code regelmäßig integriert, automatisch getestet und bei erfolgreichen Tests automatisch bereitgestellt. Dies ermöglicht eine effizientere, qualitativ hochwertige und schnellere Entwicklung von Software.

## Pipeline und Docker Image
Die Kombination aus CI/CD-Pipeline und Docker Images erleichtert die Automatisierung, Standardisierung und Effizienz des Entwicklungs- und Bereitstellungsprozesses. Schlüsselaspekte dabei sind: Build, Test, Release, Bereitstellung und Validierung und Compliance. Docker bietet die Möglichkeit, Anwendungen in isolierten Containern zu verpacken, während die CI/CD-Pipeline die kontinuierliche Integration, Testautomatisierung und Bereitstellung ermöglicht.

## Gitlab Runner
- Unter dem Menü CI/CD, gehen sie auf "Runner", und klicken auf "Erweitern".
- Kopieren sie das mitgelieferte Registrierungs-Token.
- Öffnen sie Ihr Terminal oder die Eingabeaufforderung.
- Führen sie den Befehl `gitlab-runner register` im Terminal aus.
- Wenn sie dazu aufgefordert werden, fügen sie das zuvor kopierte Registrierungs-Token ein.
Und schon ist der Runner fertig.

## AWS
Befolgen sie diese Schritte in ECS:
- Melden sie sich bei AWS an, navigieren sie zu ECS und klicken sie auf "Create ECS Cluster".
- Geben sie ihrem ECS einen Namen. Dieser wird später in den Umgebungsvariablen in GitLab verwendet.
- Klicken sie auf "Create", um die Erstellung des ECS-Clusters abzuschließen.
- Drücken sie auf Ihren Cluster und scrollen sie nach unten zu Dienste.
- Klicken sie auf die Schaltfläche "Create" und konfigurieren sie den Dienst.
- Geben sie ihm einen passenden Namen, dieser wird später in den Umgebungsvariablen verwendet.


## CI/CD Variabeln
Damit die Pipeline funktioniert benötigte ich diverse CI/CD Variabeln, diese benötigen jeweils einen "key" und eine "value".
Hier sind alle Variabeln die ich für die Pipeline gebraucht habe:

Die Values von den folgenden vier Keys lässt sich im AWS Learner Lab unter AWS Details finden.
- AWS_ACCESS_KEY_ID 
- AWS_SECRET_ACCESS_KEY 
- AWS_SESSION_TOKEN         // die Values dieser drei Keys müssen immer wieder geupdated werden.

- AWS_DEFAULT_REGION  

Die Values zu diesen vier Keys lassen sich in AWS selbst finden. 
- CI_AWS_ECR_REGISTRY
- CI_AWS_ECR_REPOSITORY_NAME
- CI_AWS_ECS_CLUSTER_NAME
- CI_AWS_ECS_SERVICE_NAME

## YML
Dieses YAML-Datei ist eine GitLab CI/CD-Pipeline-Konfiguration, die ein Docker-Container auf Amazon Elastic Container Service (ECS) erstellt und bereitstellt. Die Pipeline ist in zwei Stufen unterteilt: build und deploy.

In der build-Stage führt die Pipeline die folgenden Schritte aus:

Loggt sich bei der AWS ECR-Repository in ein, indem der Befehl aws ecr get-login-password verwendet wird und das Ergebnis in der CI_AWS_ECR_LOGIN_PASSWORD Variable gespeichert wird.
Erstellt das Docker-Image mit dem docker build-Befehl, wobei es mit dem angegebenen CI_AWS_ECR_REGISTRY und CI_AWS_ECR_REPOSITORY_NAME getaggt wird.
Schiebt das erstellte Docker-Image in das ECR-Repository mit den docker tag und docker push-Befehlen.

In der deploy-Stage führt die Pipeline die folgenden Schritte aus:

Loggt sich bei der AWS ECS-Cluster in ein, indem der Befehl aws ecs update-service --cluster $CI_AWS_ECS_CLUSTER_NAME --service $CI_AWS_ECS_SERVICE_NAME --force-new-deployment verwendet wird und das Ergebnis in der CI_AWS_ECR_REGISTRY_NAME Variable gespeichert wird.
Taggt das Docker-Image im ECR-Repository mit demselben Tag wie die CI_AWS_ECR_REGISTRY und CI_AWS_ECR_REPOSITORY_NAME Variablen.

## Dockerfile
Das ist ein mehrstufiges Dockerfile, das den Java-Anwendungscode erstellt und diese in einem Docker-Container ausführt. Hier ist eine Analyse vom Dockerfile:

- FROM maven:3-openjdk-11-slim: Dies setzt die Basis-Image für die Verwendung als Maven-Image mit OpenJDK 11 und einer schlanken Version von Debian.
- ENV DB_URL="": Dies setzt eine Umgebungsvariable für die Datenbank-URL zu einem leeren String.
- ENV DB_USERNAME="": Dies setzt eine Umgebungsvariable für den Datenbank-Benutzernamen zu einem leeren String.
- ENV DB_PASSWORD="": Dies setzt eine Umgebungsvariable für das Datenbank-Passwort zu einem leeren String.
- COPY src /src: Dies kopiert den Java-Anwendungscode von der Host-Maschine zum Docker-Image, genauer gesagt in das /src-Verzeichnis im Image.
- COPY pom.xml /: Dies kopiert die Maven-Projektdatei von der Host-Maschine zum Docker-Image, genauer gesagt in das root-Verzeichnis im Image.
- RUN mvn -f pom.xml clean package: Dies führt das mvn-Kommando im Docker-Image aus, um den Java-Anwendungscode aus dem Quellcode und der Maven-Projektdatei zu erstellen.
- RUN mv /target/*.jar app.jar: Dies verschiebt die erstellte Java-Anwendungs JAR-Datei vom target-Verzeichnis in das root-Verzeichnis in das Image und benennt sie in app.jar.
- ENTRYPOINT ["java","-jar","/app.jar", "--DB_URI=${DB_URL}", "--DB_USER=${DB_USERNAME}", "--DB_PASS=${DB_PASSWORD}"]:
Dies setzt den Standardbefehl, der ausgeführt wird, wenn der Docker-Container gestartet wird. Dieser Befehl wird ausgeführt, um die Java-Anwendungs JAR-Datei auszuführen. Die Befehlszeilenargumente werden auf die Werte der Umgebungsvariablen für die Datenbank-URL, den Benutzernamen und das Passwort gesetzt.







### Reflexion
Bei diesem Auftrag habe ich sicher etwas neues gelernt auch wenn es immer wieder Schwierigkeiten gab, unteranderem mit Docker, habe ich alles bestmöglichst erledigt. Diesen Auftrag mochte ich nicht sehr da ich kaum meine eigene Kreativität miteinbringen konnte. Trotzdem war es sicher eine nützliche Aufgabe, die ich in Zukunft noch einmal benötige.
